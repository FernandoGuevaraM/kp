var catalogue = ( function() {

    /* Objeto vacío donde se guardarán los elementos del DOM que vayan a utilizarse */
    var dom = {};

    /* Objeto donde se colocan las variables que vayan a utilizarse */
    var obj = {
        catalogueInfo: "catalogue__info",
        navigationItem: "navigation__item",
        defaultOpen: "defaultOpen",
        metrics: "metrics",
        insights: "insights",
        engage: "engage",
    };

    /* Función que llena el objeto dom con los elementos html requeridos */
    var catchDom = function() {
        dom.catalogueInfo = document.getElementsByClassName(obj.catalogueInfo);
        dom.navigationItem = document.getElementsByClassName(obj.navigationItem);
        dom.defaultOpen = document.getElementById(obj.defaultOpen);
        dom.metrics = document.getElementById(obj.metrics);
        dom.insights = document.getElementById(obj.insights);
        dom.engage = document.getElementById(obj.engage);
        console.log(dom)
    };

    /* Función donde se colocan todos los eventos que tendrán los elementos */
    var suscribeEvents = function() {
        dom.navigationItem[0].addEventListener('click', function (e) {
            e.preventDefault();
            events.openProduct(e, dom.metrics);
        });
        dom.navigationItem[1].addEventListener('click', function (e) {
            e.preventDefault();
            events.openProduct(e, dom.insights);
        });
        dom.navigationItem[2].addEventListener('click', function (e) {
            e.preventDefault();
            events.openProduct(e, dom.engage);
        });
    };

    /* Objeto donde se guardan los métodos (o callbacks) de suscribeEvents */
    var events = {
        openProduct: function (evt, content) {
            for (i = 0; i < dom.catalogueInfo.length; i++) {
                dom.catalogueInfo[i].style.display = "none";
            }

            for (i = 0; i < dom.navigationItem.length; i++) {
                dom.navigationItem[i].className = dom.navigationItem[i].className.replace(" active", "");
            }
            content.style.display = "flex";
            console.log(evt);
            console.log(evt.currentTarget);
            console.log(this);
            evt.currentTarget.className += " active";
        }
    };

    /* Función que inicializa las funciones descritas previamente */
    var initialize = function() {
        catchDom();
        suscribeEvents();
    };

    /* Retornamos un objeto con el método init, haciendo referencia a la función initialize*/
    return {
        init: initialize
    };

})();

catalogue.init();
